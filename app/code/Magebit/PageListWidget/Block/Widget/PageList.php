<?php

namespace Magebit\PageListWidget\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class PageList extends Template implements BlockInterface
{

    const DISPLAY_MODE_SPECIFIC = 'specific';

    protected $_template = "page-list.phtml";
    protected $pageRepositoryInterface;
    protected $searchCriteriaBuilder;

    public function __construct(
        Template\Context $context,
        array $data = [],
        \Magento\Cms\Api\PageRepositoryInterface $pageRepositoryInterface,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        parent::__construct($context, $data);

        $this->pageRepositoryInterface = $pageRepositoryInterface;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    public function getPageList()
    {
        if ($this->getData('display_mode') == self::DISPLAY_MODE_SPECIFIC) {
            $searchCriteria = $this->searchCriteriaBuilder
                ->addFilter('identifier', $this->getData('selected_pages'), 'in')
                ->create();
        } else {
            $searchCriteria = $this->searchCriteriaBuilder->create();
        }

        return $this->pageRepositoryInterface->getList($searchCriteria)->getItems();
    }
}