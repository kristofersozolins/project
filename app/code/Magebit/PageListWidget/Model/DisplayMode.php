<?php

namespace Magebit\PageListWidget\Model;

class DisplayMode implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [
            ['value' => 'all', 'label' => __('All pages')],
            ['value' => 'specific', 'label' => __('Specific pages')],
        ];
    }
}