<?php

namespace Magebit\PageListWidget\Model;

class Pages implements \Magento\Framework\Option\ArrayInterface
{

    protected $pageRepositoryInterface;
    protected $searchCriteriaBuilder;

    public function __construct(
        \Magento\Cms\Api\PageRepositoryInterface $pageRepositoryInterface,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->pageRepositoryInterface = $pageRepositoryInterface;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    public function toOptionArray()
    {
        $pages = [];

        foreach ($this->pageRepositoryInterface->getList($this->searchCriteriaBuilder->create())->getItems() as $page) {
            $pages[] = [
                'value' => $page->getIdentifier(),
                'label' => $page->getTitle(),
            ];
        }

        return $pages;
    }
}