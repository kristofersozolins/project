<?php

namespace Magebit\Faq\Api;

use Magebit\Faq\Api\Data\QuestionInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;

interface QuestionRepositoryInterface
{
    /**
     * @param int $id
     * @return \Magebit\Faq\Api\Data\QuestionInterface
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get($id): QuestionInterface;

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Magento\Framework\Api\SearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface;

    /**
     * @param \Magebit\Faq\Api\Data\QuestionInterface $question
     * @return \Magebit\Faq\Api\Data\QuestionInterface
     *
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(QuestionInterface $question): QuestionInterface;

    /**
     * @param \Magebit\Faq\Api\Data\QuestionInterface $question
     * @return true
     *
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     */
    public function delete(QuestionInterface $question): bool;

    /**
     * @param int $id
     * @return true
     *
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     */
    public function deleteById($id): bool;
}