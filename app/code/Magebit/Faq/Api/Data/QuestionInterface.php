<?php

namespace Magebit\Faq\Api\Data;

interface QuestionInterface
{
    public const TABLE = 'magebit_faq_question';

    public const ID = 'id';
    public const QUESTION = 'question';
    public const ANSWER = 'answer';
    public const STATUS = 'status';
    public const POSITION = 'position';
    public const UPDATED_AT = 'updated_at';

    public const STATUS_ENABLED = 1;
    public const STATUS_DISABLED = 0;
    /**
     * @return string
     */
    public function getQuestion(): string;

    /**
     * @param string $value
     * @return void
     */
    public function setQuestion(string $value): void;

    /**
     * @return string
     */
    public function getAnswer(): string;

    /**
     * @param string $value
     * @return void
     */
    public function setAnswer(string $value): void;

    /**
     * @return int
     */
    public function getStatus(): int;

    /**
     * @param int $status
     * @return void
     */
    public function setStatus(int $status): void;

    /**
     * @return int
     */
    public function getPosition(): int;

    /**
     * @param int $value
     * @return void
     */
    public function setPosition(int $value): void;
}