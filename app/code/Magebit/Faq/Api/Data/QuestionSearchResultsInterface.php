<?php

namespace Magebit\Faq\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface QuestionSearchResultsInterface extends SearchResultsInterface
{
    /**
     * @return \Magebit\Faq\Api\Data\QuestionInterface[]
     */
    public function getItems(): array;

    /**
     * @param \Magebit\Faq\Api\Data\QuestionInterface[] $items
     * @return SearchResultsInterface
     */
    public function setItems(array $items): SearchResultsInterface;
}