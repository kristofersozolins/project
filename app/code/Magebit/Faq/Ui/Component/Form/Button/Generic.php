<?php

declare(strict_types=1);

namespace Magebit\Faq\Ui\Component\Form\Button;

/**
 * Class GenericButton
 */
class Generic
{
    /** @var \Magento\Framework\App\RequestInterface */
    protected $request;

    /** @var \Magebit\Faq\Api\QuestionRepositoryInterface */
    protected $questionRepository;

    /** @var \Magento\Framework\UrlInterface */
    protected $urlBuilder;

    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        \Magebit\Faq\Api\QuestionRepositoryInterface $questionRepository,
        \Magento\Framework\UrlInterface $urlBuilder
    ) {
        $this->request = $request;
        $this->questionRepository = $questionRepository;
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * @return int|null
     */
    public function getId()
    {
        try {
            return $this->questionRepository->get(
                $this->request->getParam('id')
            )->getId();
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
        }

        return null;
    }

    /**
     * @param string $route
     * @param array $params
     * @return string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->urlBuilder->getUrl($route, $params);
    }
}
