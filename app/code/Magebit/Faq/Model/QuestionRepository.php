<?php

declare(strict_types=1);

namespace Magebit\Faq\Model;

use Magebit\Faq\Api\Data\QuestionInterface;
use Magebit\Faq\Api\QuestionRepositoryInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

class QuestionRepository implements QuestionRepositoryInterface
{
    /** @var QuestionFactory */
    private $questionFactory;
    /** @var \Magebit\Faq\Api\Data\QuestionSearchResultsInterfaceFactory */
    private $searchResultFactory;
    /** @var ResourceModel\Question\CollectionFactory */
    private $questionCollectionFactory;
    /** @var CollectionProcessorInterface */
    private $collectionProcessor;
    /** @var ResourceModel\Question */
    private $resourceModel;

    /**
     * QuestionRepository constructor.
     * @param QuestionFactory $questionFactory
     * @param \Magebit\Faq\Api\Data\QuestionSearchResultsInterfaceFactory $searchResultFactory
     * @param ResourceModel\Question\CollectionFactory $questionCollectionFactory
     * @param CollectionProcessorInterface $collectionProcessor
     * @param ResourceModel\Question $resourceModel
     */
    public function __construct(
        QuestionFactory $questionFactory,
        \Magebit\Faq\Api\Data\QuestionSearchResultsInterfaceFactory $searchResultFactory,
        ResourceModel\Question\CollectionFactory $questionCollectionFactory,
        CollectionProcessorInterface $collectionProcessor,
        ResourceModel\Question $resourceModel
    ) {
        $this->questionFactory = $questionFactory;
        $this->searchResultFactory = $searchResultFactory;
        $this->questionCollectionFactory = $questionCollectionFactory;
        $this->collectionProcessor = $collectionProcessor;
        $this->resourceModel = $resourceModel;
    }

    /**
     * @inheritdoc
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface
    {
        $collection = $this->questionCollectionFactory->create();
        $this->collectionProcessor->process($searchCriteria, $collection);

        $searchResults = $this->searchResultFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());

        return $searchResults;
    }

    /**
     * @inheritdoc
     */
    public function get($id): QuestionInterface
    {
        $question = $this->questionFactory->create();
        $this->resourceModel->load($question, $id);

        if (!$question->getId()) {
            throw new NoSuchEntityException;
        }

        return $question;
    }

    /**
     * @inheritdoc
     */
    public function save(QuestionInterface $question): QuestionInterface
    {
        try {
            $this->resourceModel->save($question);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }

        return $question;
    }

    /**
     * @inheritdoc
     */
    public function delete(QuestionInterface $question): bool
    {
        try {
            $this->resourceModel->delete($question);
        } catch (\Exception $e) {
            throw new CouldNotDeleteException(__($e->getMessage()));
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function deleteById($id): bool
    {
        $this->delete($this->get($id));
    }
}