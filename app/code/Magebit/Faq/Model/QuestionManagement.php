<?php

declare(strict_types=1);

namespace Magebit\Faq\Model;

use Magebit\Faq\Api\Data\QuestionInterface;
use Magebit\Faq\Api\QuestionManagementInterface;
use Magebit\Faq\Api\QuestionRepositoryInterface;

class QuestionManagement implements QuestionManagementInterface
{
    /** @var QuestionRepositoryInterface */
    protected $questionRepository;

    public function __construct(QuestionRepositoryInterface $questionRepository)
    {
        $this->questionRepository = $questionRepository;
    }

    /**
     * @inheritdoc
     */
    public function enableQuestion(QuestionInterface $question): QuestionInterface
    {
        $question->setStatus(QuestionInterface::STATUS_ENABLED);
        $this->questionRepository->save($question);

        return $question;
    }

    /**
     * @inheritdoc
     */
    public function disableQuestion(QuestionInterface $question): QuestionInterface
    {
        $question->setStatus(QuestionInterface::STATUS_DISABLED);
        $this->questionRepository->save($question);

        return $question;
    }
}