<?php

declare(strict_types=1);

namespace Magebit\Faq\Model\Question\Source;

class IsActive implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * @var \Magebit\Faq\Model\Question
     */
    protected $question;

    /**
     * Constructor
     *
     * @param \Magebit\Faq\Model\Question $question
     */
    public function __construct(\Magebit\Faq\Model\Question $question)
    {
        $this->question = $question;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [
                'label' => 'Enabled',
                'value' => \Magebit\Faq\Api\Data\QuestionInterface::STATUS_ENABLED
            ],
            [
                'label' => 'Disabled',
                'value' => \Magebit\Faq\Api\Data\QuestionInterface::STATUS_DISABLED
            ]
        ];
    }
}
