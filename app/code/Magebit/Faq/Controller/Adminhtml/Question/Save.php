<?php

declare(strict_types=1);

namespace Magebit\Faq\Controller\Adminhtml\Question;

use Magebit\Faq\Api\Data\QuestionInterface;
use Magebit\Faq\Api\QuestionRepositoryInterface;
use Magebit\Faq\Model\Question;
use Magebit\Faq\Model\QuestionFactory;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Exception\LocalizedException;

/**
 * Save Question action.
 */
class Save extends \Magento\Backend\App\Action implements HttpPostActionInterface
{
    /** @var QuestionFactory  */
    private $questionFactory;

    /** @var QuestionRepositoryInterface  */
    private $questionRepository;

    public function __construct(
        Context $context,
        QuestionFactory $questionFactory,
        QuestionRepositoryInterface $questionRepository
    ) {
        $this->questionFactory = $questionFactory;
        $this->questionRepository = $questionRepository;
        parent::__construct($context);
    }

    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();

        if ($data) {
            if (isset($data[QuestionInterface::STATUS]) && $data[QuestionInterface::STATUS] === 'true') {
                $data[QuestionInterface::STATUS] = QuestionInterface::STATUS_ENABLED;
            }

            if (empty($data[QuestionInterface::ID])) {
                $data[QuestionInterface::ID] = null;
            }

            /** @var Question $model */
            $model = $this->questionFactory->create();

            $id = $this->getRequest()->getParam(QuestionInterface::ID);
            if ($id) {
                try {
                    $model = $this->questionRepository->get($id);
                } catch (LocalizedException $e) {
                    $this->messageManager->addErrorMessage(__('This question no longer exists'));
                }
            }

            $model->setData($data);

            try {
                $this->questionRepository->save($model);
                $this->messageManager->addSuccessMessage(__('The question has been saved'));

                $redirect = $data['back'] ?? 'continue';

                if ($redirect === 'continue') {
                    $resultRedirect->setPath('*/*/edit', ['id' => $model->getId()]);
                } else if ($redirect === 'close') {
                    $resultRedirect->setPath('*/*/');
                }

                return $resultRedirect;
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the question'));
            }
        }

        return $resultRedirect->setPath('*/*/');
    }
}
