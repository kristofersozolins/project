<?php

namespace Magebit\Faq\Block;

use Magebit\Faq\Api\Data\QuestionInterface;
use Magebit\Faq\Model\QuestionRepository;
use Magento\Framework\Api\SortOrderBuilder;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\View\Element\Template;

class QuestionList extends Template
{
    private $questionRepository;
    private $searchCriteriaBuilder;
    private $sortOrderBuilder;

    public function __construct(
        Template\Context $context,
        QuestionRepository $questionRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        SortOrderBuilder $sortOrderBuilder,
        array $data = []
    ) {
        $this->questionRepository = $questionRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->sortOrderBuilder = $sortOrderBuilder;
        parent::__construct($context, $data);
    }

    /**
     * @return \Magebit\Faq\Model\Question[]
     */
    public function getQuestions()
    {
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter(QuestionInterface::STATUS, QuestionInterface::STATUS_ENABLED)
            ->setSortOrders([
                $this->sortOrderBuilder
                    ->setField(QuestionInterface::POSITION)
                    ->setDescendingDirection()
                    ->create(),
            ])
            ->create();

        return $this->questionRepository->getList($searchCriteria)->getItems();
    }
}